module.exports = {
	testRegex: '/src/.*?(test)\\.js$',
	modulePathIgnorePatterns: ['node_modules', 'dist'],
	setupFiles: ['<rootDir>/jest-setup/index.js'],
	snapshotSerializers: ['enzyme-to-json/serializer'],
};
