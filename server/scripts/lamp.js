let tempMemory = {
	color: {
		r: 0,
		g: 0,
		b: 0,
		a: 0,
	},
	state: 'off',
};

module.exports = app => {
	app.post('/color', (req, res) => {
		tempMemory.color = req.body;
		res.sendStatus(200);
	});
	app.post('/off', (req, res) => {
		tempMemory.state = 'off';
		res.sendStatus(200);
	});
	app.post('/on', (req, res) => {
		tempMemory.state = 'on';
		res.sendStatus(200);
	});
	app.get('/state', (req, res) => {
		res.send(tempMemory);
	});
};
