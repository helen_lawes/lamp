const bodyParser = require('body-parser');
const Bundler = require('parcel-bundler');
const express = require('express');
const path = require('path');
// check if NODE_ENV is dev mode
const isDev = process.env.NODE_ENV === 'dev';

// configuration of parcel bundler
const bundler = new Bundler(path.resolve(__dirname, '../src/index.html'));

// initialise the express app
const app = express();

// pass incoming data into JSON
app.use(bodyParser.json());

// // load api routing
const api = express();
require('./scripts/lamp')(api);
app.use('/lamp', api);

// only use bundler middleware for dev environment otherwise load the built react app
if (isDev) {
	app.use(bundler.middleware());
} else {
	app.use(express.static(path.resolve(__dirname, '../dist')));
	app.get('/*', (req, res) => {
		res.sendFile(path.join(__dirname, '../dist', 'index.html'));
	});
}

// set server to listen to a port
app.listen(process.env.PORT || 3000);
