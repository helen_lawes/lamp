from microbit import *
import radio

REFRESH = 10
CHANNEL = 14
POWER = 7

radio.on()
radio.config(channel=CHANNEL)
radio.config(power=POWER)


def get_data():
    bytestring = uart.readline()
    string = str(bytestring)
    radio.send(string)


def get_sensor_data():
    a, b = button_a.was_pressed(), button_b.was_pressed()
    print(a, b)


def run():
    while True:
        sleep(REFRESH)
        get_sensor_data()
        get_data()


display.scroll('hello')

run()
