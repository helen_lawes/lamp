from microbit import *
import neopixel
import radio

REFRESH = 10
NUM_PIXELS = 24
CHANNEL = 14
POWER = 7

np = neopixel.NeoPixel(pin0, NUM_PIXELS)
radio.on()
radio.config(channel=CHANNEL)
radio.config(power=POWER)


def setPixelColor(r, g, b, brightness):  # brightness from 0-1
    return (int(brightness*r), int(brightness*g), int(brightness*b))


def get_data():
    string = radio.receive()
    if string is not None:
        string = string[2:-1]  # remove the b'' prefix
        if string == 'off':
            np.clear()
            np.show()
        else:
            data_list = string.split('-')
            try:
                r, g, b, a = data_list

                for pixel_id in range(0, len(np)):
                    red = int(r)
                    green = int(g)
                    blue = int(b)
                    brightness = float(a)

                    color = setPixelColor(red, green, blue, brightness)
                    print(color)
                    np[pixel_id] = color
                np.show()
            except:
                pass


def run():
    while True:
        sleep(REFRESH)
        get_data()


display.scroll('hello')

run()
