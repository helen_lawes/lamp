import time
import random
import board
import adafruit_dotstar as dotstar
import sys
import getopt


def main(argv):
    r = ''
    g = ''
    b = ''
    a = ''
    try:
        opts, args = getopt.getopt(
            argv, "hr:g:b:a:", ["red=", "green=", "blue=", "alpha="])
    except getopt.GetoptError:
        print 'main.py -r <redChannel> -g <greenChannel> -b <blueChannel> -a <alphaChannel>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'main.py -r <redChannel> -g <greenChannel> -b <blueChannel> -a <alphaChannel>'
            sys.exit()
        elif opt in ("-r", "--red"):
            r = int(arg)
        elif opt in ("-g", "--green"):
            g = int(arg)
        elif opt in ("-b", "--blue"):
            b = int(b)
        elif opt in ("-a", "--alpha"):
            a = float(arg)
    if (r & g & b & a):
        color = (r, g, b)
        # Using a DotStar Digital LED Strip with 30 LEDs connected to hardware SPI
        dots = dotstar.DotStar(board.SCK, board.MOSI, 30, brightness=a)

        dots.fill(color)


if __name__ == "__main__":
    main(sys.argv[1:])
