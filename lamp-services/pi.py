import serial

PORT = "/dev/ttyACM0"

BAUD = 115200

REFRESH = 500

colors = [
    [255, 0, 0],
    [255, 127, 0],
    [255, 255, 0],
    [127, 255, 0],
    [0, 255, 0],
    [0, 255, 127],
    [0, 255, 255],
    [0, 127, 255],
    [0, 0, 255],
    [127, 0, 255],
    [255, 0, 255],
    [255, 0, 127],
]

s = serial.Serial(PORT)
s.baudrate = BAUD
s.parity = serial.PARITY_NONE
s.databits = serial.EIGHTBITS
s.stopbits = serial.STOPBITS_ONE

color = 0

print('started')

while True:
    if color == -1:
        s.write('off')
    else:
        r, g, b = colors[color]
        s.write('%d-%d-%d' % (r, g, b))
        print('sent-color', r, g, b)
    data = s.readline().decode('UTF-8')
    print('data:', data)
    data_list = data.rstrip().split(' ')
    try:
        a, b = data_list
        if a == 'True' and b == 'True':
            color = -1
        elif a == 'True':
            color -= 1
            if color < 0:
                color = len(colors) - 1
            r, g, b = colors[color]
            print(r, g, b)
        elif b == 'True':
            color += 1
            if color >= len(colors):
                color = 0
            r, g, b = colors[color]
            print(r, g, b)
    except:
        pass

s.close()
