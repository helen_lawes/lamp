def on_data_received():
    global string
    string = serial.read_until(serial.delimiters(Delimiters.COMMA))
    radio.send_string(string)


serial.on_data_received(serial.delimiters(Delimiters.COMMA), on_data_received)

string = ""
radio.set_group(14)
radio.set_transmit_power(7)
serial.set_baud_rate(BaudRate.BAUD_RATE115200)
basic.show_icon(IconNames.GIRAFFE)
basic.pause(300)
basic.clear_screen()
