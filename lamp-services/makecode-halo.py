def on_received_string(string):
    if string != None:
        if string == "off":
            strip.clear()
            strip.show()
        else:
            data_list = helpers.string_split(string, "-")
            try:
                r, g, b, a = data_list
                red = int(r)
                green = int(g)
                blue = int(b)
                brightness = parse_float(a) * 255
                strip.set_brightness(brightness)
                strip.show_color(neopixel.rgb(red, green, blue))

                strip.show()
            except:
                basic.show_string("error")

                pass


radio.on_received_string(on_received_string)

strip: neopixel.Strip = None
radio.set_group(14)
radio.set_transmit_power(7)
serial.set_baud_rate(BaudRate.BAUD_RATE115200)
strip = neopixel.create(DigitalPin.P0, 24, NeoPixelMode.RGB)
strip.set_brightness(50)
strip.show_rainbow()
basic.pause(300)
strip.clear()
strip.show()
