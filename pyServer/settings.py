def retrieve_color():
    with open('color.txt', 'r') as f:
        color = f.read()
        r, g, b, a = color.split(',')
        return { 'r': int(r), 'g': int(g), 'b': int(b), 'a': float(a)}

def save_color(r, g, b, a):
    color = f'{r},{g},{b},{a}'
    with open('color.txt', 'w') as f:
        f.write(color)

def retrieve_state():
    with open('state.txt', 'r') as f:
        return f.read()

def save_state(state):
    with open('state.txt', 'w') as f:
        f.write(state);
