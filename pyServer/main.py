from flask import Flask, request
from lamp import change_color, turn_off
from settings import retrieve_color, save_color, retrieve_state, save_state

app = Flask(__name__, static_url_path='',
            static_folder='../dist')


@app.route('/')
def root():
    return app.send_static_file('index.html')

@app.route('/lamp/state')
def settings():
    color = retrieve_color()
    state = retrieve_state()
    return { 'color': color, 'state': state }

@app.route('/lamp/color', methods=['POST'])
def lamp():
    r = request.json['r']
    g = request.json['g']
    b = request.json['b']
    a = request.json['a']
    state = retrieve_state()
    if (state == 'on'):
        change_color(r, g, b, a)
    save_color(r, g, b, a)
    return 'ok'


@app.route('/lamp/off', methods=['POST'])
def off():
    turn_off()
    save_state('off')
    return 'ok'

@app.route('/lamp/on', methods=['POST'])
def on():
    save_state('on')
    color = retrieve_color()
    change_color(color['r'], color['g'], color['b'], color['a'])
    return 'ok'
