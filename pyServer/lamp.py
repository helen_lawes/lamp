import board
import adafruit_dotstar as dotstar


def change_color(r, g, b, a):
    color = (r, g, b)
    # Using a DotStar Digital LED Strip with 30 LEDs connected to hardware SPI
    dots = dotstar.DotStar(board.SCK, board.MOSI, 30, brightness=a)
    dots.fill(color)


def turn_off():
    dots = dotstar.DotStar(board.SCK, board.MOSI, 30, brightness=0)
    dots.deinit()
