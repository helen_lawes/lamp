import styled, { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
	body {
		background: ${({ r, g, b, a }) => `rgba(${r}, ${g}, ${b}, ${a})`};
	}
`;

const StyledApp = styled.div`
	max-width: 600px;
	margin: 0 auto;
	font-family: sans-serif;
	* {
		box-sizing: border-box;
	}
`;

export default StyledApp;

export const LightToggle = styled.div`
	width: 400px;
	max-width: calc(100vw - 100px);
	margin: 20px auto;
`;
