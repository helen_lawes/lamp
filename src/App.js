import React, { useEffect, useState } from 'react';

import api from './api';
import { useDebounce } from './hooks/useDebounce';

import StyledApp, { GlobalStyle, LightToggle } from './App.styles';
import ColourPicker from './components/ColourPicker/ColourPicker';
import { Toggle } from './components/Toggle/Toggle';

const setLamp = ({ r, g, b, a }) => {
	if (r === 0 && g === 0 && b === 0) api.off();
	else api.setColour(r, g, b, a / 10);
};

const setLampState = lampOn => {
	if (lampOn) api.on();
	else api.off();
};

const App = () => {
	const [rgba, setRgba] = useState({});
	const [lampOn, setLampOn] = useState(false);
	const debounceRgba = useDebounce(rgba, 300);
	const onLightToggle = value => {
		setLampOn(value);
		setLampState(value);
	};
	useEffect(() => {
		if (!Object.keys(debounceRgba).length) return;
		setLamp(debounceRgba);
	}, [debounceRgba]);
	useEffect(() => {
		const getState = async () => {
			const state = await api.getState();
			console.log(state);
			setRgba(state.color);
			setLampOn(state.state === 'on');
		};
		console.log('get state');
		getState();
	}, []);
	return (
		<StyledApp>
			<GlobalStyle {...rgba} />
			<LightToggle>
				<Toggle
					label="Light"
					checked={lampOn}
					onChange={onLightToggle}
				/>
			</LightToggle>
			<ColourPicker rgba={rgba} setRgba={setRgba} />
		</StyledApp>
	);
};

export default App;
