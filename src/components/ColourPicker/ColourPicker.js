import React from 'react';
import PropTypes from 'prop-types';
import { RgbaColorPicker } from 'react-colorful';
import 'react-colorful/dist/index.css';

import StyledColourPicker, { Colour, Palette } from './ColourPicker.styles';

const ColourPicker = ({ rgba, setRgba }) => {
	const colours = [
		[255, 0, 0],
		[255, 127, 0],
		[255, 255, 0],
		[0, 255, 0],
		[0, 255, 255],
		[0, 0, 255],
		[120, 63, 178],
		[255, 0, 255],
		[255, 0, 127],
	];

	return (
		<StyledColourPicker>
			<RgbaColorPicker color={rgba} onChange={setRgba} />
			<Palette>
				<Colour
					red={0}
					green={0}
					blue={0}
					onClick={() => setRgba({ r: 0, g: 0, b: 0, a: rgba.a })}
				/>
				<Colour
					red={255}
					green={255}
					blue={255}
					onClick={() =>
						setRgba({ r: 255, g: 255, b: 255, a: rgba.a })
					}
				/>
				{colours.map(([red, green, blue], i) => (
					<Colour
						key={i}
						{...{ red, green, blue }}
						onClick={() =>
							setRgba({ r: red, g: green, b: blue, a: rgba.a })
						}
					/>
				))}
			</Palette>
		</StyledColourPicker>
	);
};

ColourPicker.description = `
	ColourPicker component
`;

ColourPicker.propTypes = {
	rgba: PropTypes.shape({
		r: PropTypes.number,
		g: PropTypes.number,
		b: PropTypes.number,
		a: PropTypes.number,
	}),
	/** to update colour */
	setRgba: PropTypes.func,
};

export default ColourPicker;
