import styled from 'styled-components';

const StyledColourPicker = styled.div`
	.react-colorful {
		width: 400px;
		height: initial;
		max-width: calc(100vw - 100px);
		aspect-ratio: 1 / 1;
		margin: 40px auto;
	}
`;

export default StyledColourPicker;

export const Colour = styled.div.attrs(({ red, green, blue }) => ({
	style: {
		background: `rgb(${red}, ${green}, ${blue})`,
	},
}))`
	width: 45px;
	height: 45px;
	border-radius: 100%;
	border: 1px solid #7d7d7d;
	${props =>
		props.onClick &&
		`
		cursor: pointer;
	`}
	${props =>
		props.selected &&
		`
		border-radius: 8px;
		border: 2px solid #000;
	`}
`;

export const Palette = styled.div`
	display: flex;
	flex-wrap: wrap;
	padding-top: 10px;
	width: 400px;
	max-width: calc(100vw - 80px);
	margin: 0 auto;
	${Colour} {
		margin: 5px;
	}
`;
