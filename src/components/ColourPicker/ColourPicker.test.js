import React from 'react';
import { shallow } from 'enzyme';
import ColourPicker from './ColourPicker';

describe('<ColourPicker />', () => {
	test('Should render without error', () => {
		const component = shallow(<ColourPicker />);

		expect(component).toMatchSnapshot();
	});
});
