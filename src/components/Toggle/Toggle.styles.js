import styled from 'styled-components';
import { css } from 'styled-components';

const size = 30;
const padding = 2;
const buttonSize = size - padding * 2;
const track = 70;

export const ToggleButton = styled.div`
	background: #fff;
	width: ${buttonSize}px;
	height: ${buttonSize}px;
	border-radius: 100%;
	position: absolute;
	left: ${padding}px;
	top: ${padding}px;
	transform: translate(0, 0);
	transition: 0.5s ease-in-out;
`;

export const ToggleWrapper = styled.div`
	position: relative;
	width: ${track}px;
	height: ${size}px;
	border-radius: ${size / 2}px;
	background: #333;
	transition: 0.5s ease-in-out;
	cursor: pointer;
	display: inline-block;
	vertical-align: middle;
	${({ $on }) =>
		$on &&
		css`
			background: #1dab84;
		`}
	${ToggleButton} {
		${({ $on }) =>
			$on &&
			css`
				transform: translate(${track - size}px, 0);
			`}
	}
`;

export const ToggleLabel = styled.div`
	display: inline-block;
	margin-right: 10px;
	vertical-align: middle;
`;
