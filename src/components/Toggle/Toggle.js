import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { ToggleWrapper, ToggleButton, ToggleLabel } from './Toggle.styles';

export const Toggle = ({ label, checked, onChange }) => {
	const [on, setOn] = useState(checked);

	const onClick = () => {
		const newValue = !on;
		onChange && onChange(newValue);
		setOn(newValue);
	};

	useEffect(() => {
		setOn(checked);
	}, [checked]);

	return (
		<div>
			{label && <ToggleLabel>{label}</ToggleLabel>}
			<ToggleWrapper $on={on} onClick={onClick}>
				<ToggleButton />
			</ToggleWrapper>
		</div>
	);
};

Toggle.propTypes = {
	label: PropTypes.string,
	onChange: PropTypes.func,
	checked: PropTypes.bool,
};
