import React from 'react';
import { shallow } from 'enzyme';
import Brightness from './Brightness';

describe('<Brightness />', () => {
	test('Should render without error', () => {
		const component = shallow(<Brightness />);

		expect(component).toMatchSnapshot();
	});
});
