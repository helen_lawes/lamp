import styled from 'styled-components';
import chroma from 'chroma-js';

const borderRadius = '6px';
const primaryColor = '#00225f';
const handle = 35;
const handleBorder = 2;
const track = 6;
const dot = 15;
const tint = (color, percent) => {
	return chroma.mix(color, '#fff', percent / 100);
};

const bright = (color, brightness) => {
	return chroma.mix('#fff', color, brightness / 10);
};

const darken = colour => chroma(colour).darken();

const StyledBrightness = styled.div`
	padding: 40px 20px;
	.rc-slider {
		position: relative;
		height: ${handle}px;
		width: 100%;
		border-radius: ${borderRadius};
		touch-action: none;

		&-rail {
			position: absolute;
			width: 100%;
			background-color: #e9e9e9;
			height: ${track}px;
			border-radius: ${borderRadius};
		}

		&-track {
			position: absolute;
			left: 0;
			height: ${track}px;
			border-radius: ${borderRadius};
			background-color: ${props => darken(props.colour)};
		}

		&-handle {
			position: absolute;
			width: ${handle}px;
			height: ${handle}px;
			cursor: pointer;
			cursor: -webkit-grab;
			margin-top: -${(handle - track) / 2}px;
			cursor: grab;
			border-radius: 50%;
			background: ${props => bright(props.colour, props.brightness)};
			border: solid ${handleBorder}px ${props => darken(props.colour)};
			touch-action: pan-x;

			&:focus {
				border-color: ${tint(primaryColor, 20)};
				box-shadow: 0 0 0 5px ${tint(primaryColor, 50)};
				outline: none;
			}

			&-click-focused:focus {
				border-color: ${tint(primaryColor, 50)};
				box-shadow: unset;
			}

			&:hover {
				border-color: ${tint(primaryColor, 20)};
			}

			&:active {
				border-color: ${tint(primaryColor, 20)};
				box-shadow: 0 0 5px ${tint(primaryColor, 20)};
				cursor: -webkit-grabbing;
				cursor: grabbing;
			}
		}

		&-mark {
			position: absolute;
			top: ${track + handle}px;
			left: 0;
			width: 100%;
			font-size: 12px;
		}

		&-mark-text {
			position: absolute;
			display: inline-block;
			vertical-align: middle;
			text-align: center;
			cursor: pointer;
			color: #999;

			&-active {
				color: #000;
			}
		}

		&-step {
			position: absolute;
			width: 100%;
			height: ${track}px;
			background: transparent;
		}

		&-dot {
			position: absolute;
			bottom: -${(dot - track) / 2}px;
			margin-left: -2px;
			width: 4px;
			height: ${dot}px;
			border: 2px solid #e9e9e9;
			background-color: #fff;
			cursor: pointer;
			vertical-align: middle;
			&-active {
				border-color: ${props => darken(props.colour)};
				background: ${props => bright(props.colour, props.brightness)};
			}
			&-reverse {
				margin-left: 0;
				margin-right: -2px;
			}
		}
	}
`;

export default StyledBrightness;
