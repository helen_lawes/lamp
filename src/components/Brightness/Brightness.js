import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'rc-slider';

import StyledBrightness from './Brightness.styles';

const Brightness = ({ brightness, setBrightness, colour }) => {
	const marks = {
		0: '0%',
		1: '10',
		2: '20',
		3: '30',
		4: '40',
		5: '50',
		6: '60',
		7: '70',
		8: '80',
		9: '90',
		10: '100%',
	};
	return (
		<StyledBrightness colour={colour} brightness={brightness}>
			<Slider
				min={0}
				max={10}
				step={1}
				value={brightness}
				marks={marks}
				onChange={setBrightness}
			/>
		</StyledBrightness>
	);
};

Brightness.description = `
	Brightness component
`;

Brightness.propTypes = {
	/** brightness */
	brightness: PropTypes.number,
	/** set brightness */
	setBrightness: PropTypes.func,
	/** colour selected */
	colour: PropTypes.string,
};

export default Brightness;
