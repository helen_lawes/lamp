class Api {
	constructor() {
		this.baseUrl = '/lamp';
	}
	async getState() {
		const request = await fetch(`${this.baseUrl}/state`);
		if (request.ok) {
			const response = await request.json();
			const state = response;
			return {
				...state,
				color: { ...state.color, a: state.color.a * 10 },
			};
		}
		return {
			color: {
				r: 0,
				g: 0,
				b: 0,
				a: 0,
			},
			state: 'off',
		};
	}
	setColour(r, g, b, a) {
		return fetch(`${this.baseUrl}/color`, {
			method: 'post',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				r,
				g,
				b,
				a,
			}),
		});
	}
	off() {
		return fetch(`${this.baseUrl}/off`, {
			method: 'post',
		});
	}
	on() {
		return fetch(`${this.baseUrl}/on`, {
			method: 'post',
		});
	}
}

export default new Api();
